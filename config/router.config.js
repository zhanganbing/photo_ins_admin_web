export default [
  { path: '/user/login', component: './User/Login' },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      // userTable
      {
        path: '/user',
        icon: 'table',
        name: 'user',
        routes: [
          {
            path: '/user/user-list',
            name: 'userTable',
            component: './PhotoIns/user/UserTable.js',
          },
        ],
      },

      //验证码
      {
        path: '/captcha',
        icon: 'table',
        name: 'captcha',
        routes: [
          {
            path: '/captcha/captcha-list',
            name: 'captchaTable',
            component: './PhotoIns/captcha/CaptchaTable.js',
          },
        ],
      },
      //推广记录
      {
        path: '/promote',
        icon: 'table',
        name: 'promote',
        routes: [
          {
            path: '/promote/promote-list',
            name: 'promoteTable',
            component: './PhotoIns/promote/PromoteTable.js',
          },
        ],
      },
      //服务价格
      {
        path: '/service',
        icon: 'table',
        name: 'servicePrice',
        routes: [
          {
            path: '/service/servicePrice',
            name: 'servicePriceTable',
            component: './PhotoIns/service/ServicePriceTable.js',
          },
        ],
      },
      //购买金币
      {
        path: '/coin',
        icon: 'table',
        name: 'coin',
        routes: [
          {
            path: '/coin/addCoin',
            name: 'addCoinTable',
            component: './PhotoIns/coin/CoinTable.js',
          },
          {
            path: '/coin/coinUseTable',
            name: 'coinUseTable',
            component: './PhotoIns/coin/CoinUseTable.js',
          },
          {
            path: '/coin/coinUseProdTable',
            name: 'coinUseProdTable',
            component: './PhotoIns/coin/CoinUseProdTable.js',
          },
          {
            path: '/coin/coinSellTable',
            name: 'coinSellTable',
            component: './PhotoIns/coin/CoinSellTable.js',
          },
        ],
      },


      //配置
      {
        path: '/config',
        icon: 'table',
        name: 'config',
        routes: [
          {
            path: '/config/configTable',
            name: 'configTable',
            component: './PhotoIns/config/ConfigTable.js',
          },
        ],
      },

      {
        component: '404',
      },
    ], //routes
  }, //app
]; //default
