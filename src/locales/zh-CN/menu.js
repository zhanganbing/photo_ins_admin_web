export default {
  'menu.home': '首页',
  'menu.login': '登录',
  'menu.register': '注册',
  'menu.register.result': '注册结果',
  'menu.user': '用户',
  'menu.user.userTable': '用户列表',
  'menu.captcha': '验证码',
  'menu.captcha.captchaTable': '验证码列表',
  'menu.promote': '推广记录',
  'menu.promote.promoteTable': '推广记录列表',
  'menu.servicePrice': '服务价格',
  'menu.servicePrice.servicePriceTable': '服务价格表',
  'menu.coin': '金币记录',
  'menu.coin.addCoinTable': '购买金币记录表',
  'menu.coin.coinUseTable': '金币使用记录表',
  'menu.coin.coinUseProdTable': '生产环境金币使用记录',
  'menu.coin.coinSellTable': '金币价格表',
  'menu.config': '配置表',
  'menu.config.configTable': '配置记录表',

};
