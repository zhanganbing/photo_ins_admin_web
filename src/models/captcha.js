import { findAllCaptcha, queryActivities } from '@/services/api';

export default {
  namespace: 'captcha',

  state: {
    captchaList: [],
  },

  effects: {

    *findAllCaptcha({ payload }, { call, put }) {

      const response = yield call(findAllCaptcha, payload);

      console.log('-------------------findAllCaptcha-----response----------------',response);
      yield put({
        type: 'save',
        payload: response.data,
      });

    },


  },


  reducers: {
    save(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        captchaList: action.payload,
      };
    },

  },
};
