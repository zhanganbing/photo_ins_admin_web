import { addConfig, delConfig, findPackageNames, queryAllConfig, updateConfig } from '@/services/api';
import { message } from 'antd';

export default {
  namespace: 'configTable',

  state: {
    configList: {},
    addResult: false,
    updateResult: false,
    packageNameList: [],
  },

  effects: {

    * findAllConfig({ payload }, { call, put }) {

      const response = yield call(queryAllConfig, payload);

      console.log('-------------------findAllConfig-----response----------------', response);
      if (response.code === '0') {
        yield put({
          type: 'save',
          payload: response.data,
        });
      } else {
        yield put({
          type: 'save',
          payload: {},
        });
      }

    },

    * addOneConfig({ payload, callback }, { call, put }) {

      const response = yield call(addConfig, payload);

      console.log('-------------------addOneConfig-----response----------------', response);
      if (response.code === '0') {
        yield put({
          type: 'add',
          payload: response.data,
        });
      } else {
        message.error('重复的key');
        yield put({
          type: 'add',
          payload: {},
        });
      }
      if (callback) {
        callback(response);
      }

    },

    * updateOneConfig({ payload }, { call, put }) {

      const response = yield call(updateConfig, payload);

      console.log('-------------------updateOneConfig-----response----------------', response);
      if (response.code === '0') {
        yield put({
          type: 'update',
          payload: response.data,
        });
      } else {
        yield put({
          type: 'update',
          payload: {},
        });
      }

    },
    * delOneConfig({ payload, callback }, { call, put }) {

      const response = yield call(delConfig, payload);

      console.log('-------------------delOneConfig-----response----------------', response);
      if (callback) {
        callback(response);
      }
    },

    * getPackageNames({ payload, callback }, { call, put }) {

      const response = yield call(findPackageNames, payload);

      console.log('-------------------getPackageNames-----response----------------', response);
      if (response.code === '0') {
        yield put({
          type: 'packageNames',
          payload: response.data,
        });
      } else {
        yield put({
          type: 'packageNames',
          payload: {},
        });
      }

    },



  },


  reducers: {
    save(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        configList: action.payload,
      };
    },
    add(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        addResult: action.payload,
      };
    },
    update(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        updateResult: action.payload,
      };
    },
    packageNames(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        packageNameList: action.payload,
      };
    },


  },
};
