import {
  deleteCoinPrice,
  deleteGoldCoinBuyRecord,
  insertCoinPrice,
  modifyCoinPrice,
  modifyGoldCoinBuyStatus,
  qrycoinPrice,
  qryGoldUseImg,
  qryGoldUseProd,
  queryGoldCoinBuyRecord,
} from '@/services/api';

export default {
  namespace: 'goldCoin',

  state: {
    goldAddRecord: [],
    goldUseRecord: [],
    goldUseRecordProd: [],
    coinPriceRecord: [],
    allGoldUseProdRecord: [],
  },

  effects: {

    * findAllGoldCoin({ payload }, { call, put }) {

      const response = yield call(queryGoldCoinBuyRecord, payload);

      console.log('-------------------queryGoldCoinBuyRecord-----response----------------', response);
      yield put({
        type: 'save',
        payload: response.data,
      });

    },
    * modifyGoldCoinBuyStatus({ payload }, { call, put }) {

      const response = yield call(modifyGoldCoinBuyStatus, payload);

      console.log('-------------------modifyGoldCoinBuyStatus-----response----------------', response);
      const data = response.data;

    },
    * deleteGoldCoinBuyRecord({ payload }, { call, callback }) {

      const response = yield call(deleteGoldCoinBuyRecord, payload);

      console.log('-------------------deleteGoldCoinBuyRecord-----response----------------', response);
      const data = response.data;

      if (callback) {

        callback(data);
      }
    },

    * findAllGoldUseImg({ payload }, { call, put }) {
      const response = yield call(qryGoldUseImg, payload);

      console.log('-------------------qryGoldUseImg-----response----------------', response);
      yield put({
        type: 'qryAllGoldUseImg',
        payload: response.data,
      });
    },

    * findAllGoldUseProd({ payload }, { call, put }) {
      const response = yield call(qryGoldUseProd, payload);

      console.log('-------------------qryGoldUseImg-----response----------------', response);
      yield put({
        type: 'qryAllGoldUseProd',
        payload: response.data,
      });
    },
    * qrycoinPrice({ payload }, { call, put }) {
      const response = yield call(qrycoinPrice, payload);

      console.log('-------------------qrycoinPrice-----response----------------', response);
      yield put({
        type: 'qryCoinPrice',
        payload: response.data,
      });
    },

    * insertCoinPrice({ payload, callback }, { call }) {
      const response = yield call(insertCoinPrice, payload);

      console.log('-------------------qrycoinPrice-----response----------------', response);
      if (callback) {
        callback(response);
      }
    },
    * deleteCoinPrice({ payload, callback }, { call }) {
      const response = yield call(deleteCoinPrice, payload);

      console.log('-------------------qrycoinPrice-----response----------------', response);
      if (callback) {
        callback(response);
      }
    },
    * modifyCoinPrice({ payload }, { call, put }) {
      const response = yield call(modifyCoinPrice, payload);

      console.log('-------------------qrycoinPrice-----response----------------', response);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },





  },


  reducers: {
    save(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        goldAddRecord: action.payload,
      };
    },
    qryCoinPrice(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        coinPriceRecord: action.payload,
      };
    },

    qryAllGoldUseImg(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        goldUseRecord: action.payload,
      };
    },
    qryAllGoldUseProd(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        goldUseRecordProd: action.payload,
      };
    },


  },
};
