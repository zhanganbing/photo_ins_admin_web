import { routerRedux } from 'dva/router';
import { stringify } from 'qs';
import { fakeAccountLogin, getFakeCaptcha, adminLogin } from '@/services/api';
import { setAuthority } from '@/utils/authority';
import { setCookie } from '@/utils/utils';
import { encryptParam } from '@/utils/EncryptUtils';
import { reloadAuthorized } from '@/utils/Authorized';

export default {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      console.log(payload);

      const payload2 = {
        code: encryptParam(JSON.stringify(payload)),
      };

      const response = yield call(adminLogin, payload2);

      if (response.code === '0') {
        setCookie('userName', payload.userName, 1);
        setCookie('token', response.data, 1);

        // console.log('用户名和密码');s
        // reloadAuthorized();

        yield put(routerRedux.push('/user/user-list'));
        // yield put(routerRedux.replace(redirect || '/'));
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    *logout(_, { put }) {
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          currentAuthority: 'guest',
        },
      });
      reloadAuthorized();
      // redirect
      if (window.location.pathname !== '/user/login') {
        yield put(
          routerRedux.replace({
            pathname: '/user/login',
            search: stringify({
              redirect: window.location.href,
            }),
          })
        );
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
