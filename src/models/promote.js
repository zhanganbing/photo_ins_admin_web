import { findAllPromote } from '@/services/api';

export default {
  namespace: 'promote',

  state: {
    promoteList: [],
  },

  effects: {

    *findAllPromote({ payload }, { call, put }) {

      const response = yield call(findAllPromote, payload);

      console.log('-------------------findAllPromote-----response----------------',response);
      yield put({
        type: 'save',
        payload: response.data,
      });

    },


  },


  reducers: {
    save(state, action) {
      return {
        ...state,
        promoteList: action.payload,
      };
    },

  },
};
