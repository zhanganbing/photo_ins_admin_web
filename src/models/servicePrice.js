import { deleteOneServicePrice, findAllServicePrice, modifyOneServicePrice, saveServicePrice } from '@/services/api';

export default {
  namespace: 'servicePrice',

  state: {
    servicePriceList: [],
    saveResult: {},
    deleteResult: {},
    modifyResult: {},
  },

  effects: {

    * findAllServicePrice({ payload }, { call, put }) {

      const response = yield call(findAllServicePrice, payload);

      console.log('-------------------servicePriceList-----response----------------', response);
      yield put({
        type: 'save',
        payload: response.data,
      });

    },
    * saveServicePrice({ payload, callback }, { call, put }) {

      const response = yield call(saveServicePrice, payload);

      console.log('-------------------saveServicePrice-----response----------------', response);
      yield put({
        type: 'savePriceResult',
        payload: response.data,
      });
      if (callback) {
        callback(response);
      }

    },
    * deleteServicePrice({ payload, callback }, { call, put }) {

      const response = yield call(deleteOneServicePrice, payload);

      console.log('-------------------deleteOneServicePrice-----response----------------', response);
      if (callback) {
        callback(response);
      }

    },
    * modifyServicePrice({ payload }, { call, put }) {

      const response = yield call(modifyOneServicePrice, payload);

      console.log('-------------------modifyOneServicePrice-----response----------------', response);
      yield put({
        type: 'modify',
        payload: response.data,
      });

    },


  },


  reducers: {
    save(state, action) {
      return {
        ...state,
        servicePriceList: action.payload,
      };
    },
    savePriceResult(state, action) {
      return {
        ...state,
        saveResult: action.payload,
      };
    },
    delete(state, action) {
      return {
        ...state,
        deleteResult: action.payload,
      };
    },
    modify(state, action) {
      return {
        ...state,
        modifyResult: action.payload,
      };
    },

  },
};
