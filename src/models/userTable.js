import { findAll, findByUser, queryActivities } from '@/services/api';
import { decryptParam, encryptParam } from '@/utils/EncryptUtils';

export default {
  namespace: 'userTable',

  state: {
    userList: {},
  },

  effects: {

    * findUsers({ payload }, { call, put }) {

      const response = yield call(findAll, payload);

      console.log('-------------------findUsers-----response----------------', response);
      if(response.code==="0"){
        yield put({
          type: 'save1',
          payload: response.data,
        });
      }else{
        yield put({
          type: 'save1',
          payload: {},
        });
      }

    },
    * findOneUser({ payload }, { call, put }) {

      const response = yield call(findByUser, payload);
      if(response.code==="0"){
        yield put({
          type: 'save1',
          payload: response.data,
        });
      }else{
        yield put({
          type: 'save1',
          payload: {},
        });
      }
    },


  },


  reducers: {
    save1(state, action) {
      console.log('-------------------action------------------', action);
      return {
        ...state,
        userList: action.payload,
      };
    },

  },
};
