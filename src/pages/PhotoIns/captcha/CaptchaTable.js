import { Form, Input, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ captcha, loading }) => ({
  captcha,
  loading: loading.models.captcha,
}))
@Form.create()
export default class CaptchaTable extends PureComponent {

  state = {
    inputValue: '',
    isSearch: 0,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'captcha/findAllCaptcha',
      payload: {
        code: encryptParam(param)
      },
    });

  }

  onShowSizeChange(current,pageSize) {

    console.log('------------onShowSizeChange--------------------', current,pageSize);


  }

  search = (inputValue) => {
    this.setState({
      isSearch: inputValue,
    });

    const { dispatch } = this.props;
    const param = '{"pageNum": 1,"pageSize": 10,"userIdOrEmailLike": ' + inputValue + '}';
    dispatch({
      type: 'captcha/findAllCaptcha',
      payload: {
        code: encryptParam(param)
      },
    });
  };

  fetch = (pagination) => {
    const { dispatch } = this.props;
    if (this.state.isSearch === 0) {

      const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
      dispatch({
        type: 'captcha/findAllCaptcha',
        payload: {
          code: encryptParam(param),
        },
      });
    } else {
      const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"userIdOrEmailLike": ' + this.state.isSearch + '}';
      dispatch({
        type: 'captcha/findAllCaptcha',
        payload: {
          code: encryptParam(param),
        },
      });
    }

  };


  render() {

    console.log('captcha-------', this.props.captcha);
    let captchas = this.props.captcha.captchaList;
    return (
      <div>
        <Search
          placeholder="请输入要查找的关键字"
          style={{ width: 300 }}
          onSearch={value => this.search(value)}
          enterButton
        />
        <Table dataSource={captchas.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: captchas.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="用户Id"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="用户邮箱"
            dataIndex="email"
            key="email"
          />
          <Column
            title="验证码"
            dataIndex="captcha"
            key="captcha"
          />
          <Column
            title="包名"
            dataIndex="packageName"
            key="packageName"
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime" render={(record) => {
            return moment(record).format('YYYY-MM-DD');
          }
          }

          />

        </Table>
      </div>
    );
  }
}

