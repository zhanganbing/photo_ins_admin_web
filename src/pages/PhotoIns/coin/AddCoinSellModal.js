import { Form, Input, Modal, Radio, Select, Switch } from 'antd';
import React, { PureComponent } from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
//下拉框的数据
let children = [];

@connect(({ goldCoin, configTable, loading }) => ({
  goldCoin,
  configTable,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class AddCoinSellModal extends PureComponent {
  state = { visible: false };

  componentDidMount() {
    const { dispatch } = this.props;

    const param = '{}';
    dispatch({
      type: 'configTable/getPackageNames',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  handleOk = (e) => {
    console.log('addServicePriceModal------handleOk----------', e);
    this.props.valueToParentForAdd('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParentForAdd('cancel');
  };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;

        const param = '{"coinNum":' + values.coinNum + ',"coinPrice":' + values.coinPrice + '' +
          ',"package_name":"' + values.packageName + '","platform":"' + values.platform + '","remark":"' + values.remark + '"' +
          ',"enabled":' + values.enabled + ',"productId":"' + values.productId + '"}';

        console.log('param:', param);
        dispatch({
          type: 'goldCoin/insertCoinPrice',
          payload: {
            code: encryptParam(param),
          },
          callback: (response) => {
            if (response.code === '0') {
              this.props.reload();
            }
          },
        });
      } else {
        console.log('金币价格添加错误-----------', err);
      }
    });

  };

  getChildren = (open) => {
    let packageNames = this.props.configTable.packageNameList;

    if (children.length <= 0) {

      for (let i = 0; i < packageNames.length; i++) {
        children.push(<Option key={i}>{packageNames[i]}</Option>);
      }
    }

    return children;
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="添加金币价格"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>


            <FormItem label="平台">
              {getFieldDecorator('platform', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <RadioGroup>
                  <Radio value={'ios'}>ios</Radio>
                  <Radio value={'android'}>android</Radio>
                </RadioGroup>,
              )}
            </FormItem>
            <FormItem label="包">
              {getFieldDecorator('packageName', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <Select labelInValue onDropdownVisibleChange={(open) => {
                  this.getChildren(open);
                }}>
                  {children}
                </Select>,
              )}
            </FormItem>
            <FormItem label="金币数量">
              {getFieldDecorator('coinNum', {
                rules: [{
                  required: true, message: '请输入金币数量',
                }],
              })(
                <Input placeholder="请输入金币数量"/>,
              )}
            </FormItem>


            <FormItem label="金币价格">
              {getFieldDecorator('coinPrice', {
                rules: [{
                  required: true, message: '请输入金币价格',
                }],
              })(
                <Input placeholder="请输入金币价格"/>,
              )}
            </FormItem>
            <FormItem label="产品ID">
              {getFieldDecorator('productId', {
                rules: [{
                  required: true, message: '请输入产品ID',
                }],
              })(
                <Input placeholder="请输入产品ID"/>,
              )}
            </FormItem>
            <FormItem label="描述">
              {getFieldDecorator('remark', {
                rules: [{
                  required: true, message: '请输入描述',
                }],
              })(
                <Input placeholder="请输入描述"/>,
              )}
            </FormItem>
            <FormItem label="是否启用">
              {getFieldDecorator('enabled', {
                rules: [{
                  required: true, message: '请输入描述',
                }],
              })(
                <Switch checkedChildren="是" unCheckedChildren="否" defaultChecked={true}/>,
              )}
            </FormItem>


          </Form>
        </Modal>
      </div>
    );
  }
}
