import { Button, Col, Form, Input, Popconfirm, Row, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';
import UpdateServicePriceModal from './UpdateCoinSellModal';
import AddCoinSellModal from './AddCoinSellModal';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ goldCoin, loading }) => ({
  goldCoin,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class CoinUseTable extends PureComponent {

  state = {
    updateCoinVisible: false,
    updateCoinId: 0,
    addCoinSellVisible: false,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{}';

    dispatch({
      type: 'goldCoin/qrycoinPrice',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  changeVisibleForUpdate = (id) => {
    this.setState({
      updateCoinVisible: true,
      updateCoinId: id,
    });
  };

  changeVisibleForAdd = () => {
    this.setState({
      addCoinSellVisible: true,
    });
  };

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);

  }

  valueToParent = (flag) => {
    if (flag === 'ok') {
      this.setState({
        updateCoinVisible: true,
      });
    } else {
      this.setState({
        updateCoinVisible: false,
      });
    }
  };
  valueToParentForAdd = (flag) => {
    if (flag === 'ok') {
      this.setState({
        addCoinSellVisible: true,
      });
    } else {
      this.setState({
        addCoinSellVisible: false,
      });
    }
  };

  fetch = (pagination, filters) => {
    const { dispatch } = this.props;

    let packageName = filters.packageName;
    let platform = filters.platform;
    let enabled = filters.enabled;

    let param;

    if (packageName !== undefined && (enabled === undefined || enabled.length === 0) && (platform === undefined || platform.length === 0)) {
      param = '{"package_name":"' + packageName[0] + '"}';
    } else if (enabled !== undefined && (packageName === undefined || packageName.length === 0) && (platform === undefined || platform.length === 0)) {
      param = '{"enabled": ' + enabled[0] + ' }';
    } else if (platform !== undefined && (packageName === undefined || packageName.length === 0) && (enabled === undefined || enabled.length === 0)) {
      param = '{"platform": "' + platform[0] + '" }';
    } else if (packageName !== undefined && platform !== undefined && (enabled === undefined || enabled.length === 0)) {
      param = '{"package_name":"' + packageName[0] + '","platform":"' + platform[0] + '"}';
    } else if (packageName !== undefined && enabled !== undefined && (platform === undefined || platform.length === 0)) {
      param = '{"package_name":"' + packageName[0] + '","enabled":' + enabled[0] + '}';
    } else if (platform !== undefined && enabled !== undefined && (packageName === undefined || packageName.length === 0)) {
      param = '{"platform":"' + platform[0] + '","enabled":' + enabled[0] + '}';
    } else if (platform !== undefined && packageName !== undefined && enabled !== undefined) {
      param = '{"platform":"' + platform[0] + '","enabled":' + enabled[0] + ',"package_name":"' + packageName[0] + '"}';
    } else {
      param = '{}';
    }
    dispatch({
      type: 'goldCoin/qrycoinPrice',
      payload: {
        code: encryptParam(param),
      },
    });


  };
  search = (inputValue) => {
    const { dispatch } = this.props;
    const param = '{"userId": ' + inputValue + ',"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/qrycoinPrice',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  deleteColumn = (id) => {
    this.setState({
      status: 1,
    });
    const { dispatch } = this.props;
    const param = '{"id": ' + id + '}';
    dispatch({
      type: 'goldCoin/deleteCoinPrice',
      payload: {
        code: encryptParam(param),
      },
      callback: (response) => {
        if (response.code === '0') {
          this.reload();
        }
      },
    });

  };

  reload = () => {
    const { dispatch } = this.props;

    const param = '{}';

    dispatch({
      type: 'goldCoin/qrycoinPrice',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {

    console.log('goldCoin-------', this.props.goldCoin);
    let coinPriceRecords = this.props.goldCoin.coinPriceRecord;
    return (
      <div>
        <Row>
          <Col span={12}>
            <Button type="primary" onClick={() => this.changeVisibleForAdd()}>新增价格</Button>
          </Col>
          <Col span={12}>

          </Col>
        </Row>
        <UpdateServicePriceModal valueToParent={this.valueToParent} visible={this.state.updateCoinVisible}
                                 updateCoinId={this.state.updateCoinId}/>
        <AddCoinSellModal valueToParentForAdd={this.valueToParentForAdd} visible={this.state.addCoinSellVisible}
                          reload={this.reload}/>


        <Table dataSource={coinPriceRecords}
               onChange={(pagination, filters) => this.fetch(pagination, filters)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: coinPriceRecords.length,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="金币数量"
            dataIndex="coinNum"
            key="coinNum"
          />
          <Column
            title="金币价格"
            dataIndex="coinPrice"
            key="coinPrice"
          />
          <Column
            title="包名"
            dataIndex="packageName"
            key="packageName"
            filters={[{ text: 'com.editor.photo', value: 'com.editor.photo' }]}
          />
          <Column
            title="平台"
            dataIndex="platform"
            key="platform"
            filters={[{ text: 'ios', value: 'ios' }, { text: 'android', value: 'android' }]}
          />
          <Column
            title="启用"
            dataIndex="enabled"
            key="enabled"
            filters={[{ text: '否', value: false }, { text: '是', value: true }]}
            render={(record) => {
              if (record === true) return '是';
              if (record === false) return '否';
            }}
          />


          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="更新时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="操作"
            dataIndex="handle"
            key="handle"
            render={(text, record, index) => {
              const id = record.id;

              return (
                <div>
                  <Popconfirm title="确定删除？" onConfirm={() => this.deleteColumn(id)}>
                    <Button type="danger">删除</Button>
                  </Popconfirm>
                  <Button type="primary" onClick={() => this.changeVisibleForUpdate(id)}>修改</Button>
                </div>
              );
            }}
          />

        </Table>
      </div>
    );
  }
}

