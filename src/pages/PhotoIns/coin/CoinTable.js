import { Button, Col, Form, Input, Popconfirm, Radio, Row, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ goldCoin, loading }) => ({
  goldCoin,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class CoinTable extends PureComponent {

  state = {
    inputValue: '',
    status: 0,
    //0表示未搜索
    searchValue: 0,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'goldCoin/findAllGoldCoin',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);


  }


  fetch = (pagination) => {
    const { dispatch } = this.props;
    if (this.state.status === 0 && this.state.searchValue === 0) {

      const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
      dispatch({
        type: 'goldCoin/findAllGoldCoin',
        payload: {
          code: encryptParam(param),
        },
      });
    } else if (this.state.status === 1) {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1}';
        dispatch({
          type: 'goldCoin/findAllGoldCoin',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldCoin',
          payload: {
            code: encryptParam(param),
          },
        });
      }

    } else {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0}';
        dispatch({
          type: 'goldCoin/findAllGoldCoin',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldCoin',
          payload: {
            code: encryptParam(param),
          },
        });
      }
    }


  };
  search = (inputValue) => {
    const { dispatch } = this.props;
    const param = '{"userId": ' + inputValue + ',"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldCoin',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchPay = () => {
    this.setState({
      status: 1,
    });
    const { dispatch } = this.props;
    const param = '{"status": 1,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldCoin',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchNotPay = () => {
    this.setState({
      status: 2,
    });
    const { dispatch } = this.props;
    const param = '{"status": 0,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldCoin',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  changeStatus = (id) => {

    const { dispatch } = this.props;
    const param = '{"id": ' + id + '}';
    dispatch({
      type: 'goldCoin/modifyGoldCoinBuyStatus',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  deleteColumn = (id) => {

    const { dispatch } = this.props;
    const param = '{"id": ' + id + '}';
    dispatch({
      type: 'goldCoin/deleteGoldCoinBuyRecord',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {

    console.log('goldCoin-------', this.props.goldCoin);
    let goldAddRecords = this.props.goldCoin.goldAddRecord;
    return (
      <div>
        <Row>
          <Col span={12}>
            <Radio.Group buttonStyle="solid">
              <Radio.Button value="pay" onClick={this.fetchPay}>已支付</Radio.Button>
              <Radio.Button value="notPay" onClick={this.fetchNotPay}>未支付</Radio.Button>
            </Radio.Group>
          </Col>
          <Col span={12}>
            <Search
              placeholder="请输入要查找的userId"
              style={{ width: 300 }}
              onSearch={value => this.search(value)}
              enterButton
            />
          </Col>
        </Row>

        <Table dataSource={goldAddRecords.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: goldAddRecords.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="用户Id"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="购买金币数量"
            dataIndex="addGoldNum"
            key="addGoldNum"
          />
          <Column
            title="购买金币花销"
            dataIndex="buyGoldMoney"
            key="buyGoldMoney"
          />
          <Column
            title="状态"
            dataIndex="status"
            key="status"
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="操作"
            dataIndex="handle"
            key="handle"
            render={(text, record) => {
              const id = record.id;

              return (
                <div>
                  <Button type="primary" onClick={() => this.changeStatus(id)}>修改状态</Button>
                  <Popconfirm title="确定删除？" onConfirm={() => this.deleteColumn(id)}>
                    <Button type="danger">删除</Button>
                  </Popconfirm>
                </div>
              );
            }}
          />

        </Table>
      </div>
    );
  }
}

