import { Col, Form, Input, Radio, Row, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ goldCoin, loading }) => ({
  goldCoin,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class CoinUseProdTable extends PureComponent {

  state = {
    inputValue: '',
    status: 0,
    //0表示未搜索
    searchValue: 0,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);


  }


  fetch = (pagination) => {
    const { dispatch } = this.props;
    if (this.state.status === 0 && this.state.searchValue === 0) {

      const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
      dispatch({
        type: 'goldCoin/findAllGoldUseProd',
        payload: {
          code: encryptParam(param),
        },
      });
    } else if (this.state.status === 1) {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1}';
        dispatch({
          type: 'goldCoin/findAllGoldUseProd',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldUseProd',
          payload: {
            code: encryptParam(param),
          },
        });
      }

    } else {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0}';
        dispatch({
          type: 'goldCoin/findAllGoldUseProd',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldUseProd',
          payload: {
            code: encryptParam(param),
          },
        });
      }
    }


  };
  search = (inputValue) => {
    const { dispatch } = this.props;
    const param = '{"userId": ' + inputValue + ',"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchLikes = () => {
    this.setState({
      status: 1,
    });
    const { dispatch } = this.props;
    const param = '{"goldUseFor": 3,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchFollowers = () => {
    this.setState({
      status: 2,
    });
    const { dispatch } = this.props;
    const param = '{"goldUseFor": 4,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchComments = () => {
    this.setState({
      status: 2,
    });
    const { dispatch } = this.props;
    const param = '{"goldUseFor": 5,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchByStatus = (value) => {
    let realValue = value.target.value;
    const { dispatch } = this.props;
    let param;
    if (realValue === 'nopay') {
      param = '{"status": 0,"pageNum": 1,"pageSize":100}';
    } else if (realValue === 'doing') {
      param = '{"status": 1,"pageNum": 1,"pageSize":100}';
    } else {
      param = '{"status": 2,"pageNum": 1,"pageSize":100}';

    }
    dispatch({
      type: 'goldCoin/findAllGoldUseProd',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {

    console.log('goldCoin-------', this.props.goldCoin);
    let goldUseRecordProds = this.props.goldCoin.goldUseRecordProd;
    return (
      <div>
        <Row>
          <Col span={8}>
            <Radio.Group buttonStyle="solid">
              <Radio.Button value="likes" onClick={this.fetchLikes}>点赞</Radio.Button>
              <Radio.Button value="followers" onClick={this.fetchFollowers}>粉丝</Radio.Button>
              <Radio.Button value="comments" onClick={this.fetchComments}>评论</Radio.Button>
            </Radio.Group>
          </Col>
          <Col span={8}>
            <Radio.Group buttonStyle="solid">
              <Radio.Button value="nopay" onClick={(value) => this.fetchByStatus(value)}>未支付</Radio.Button>
              <Radio.Button value="doing" onClick={(value) => this.fetchByStatus(value)}>执行中</Radio.Button>
              <Radio.Button value="done" onClick={(value) => this.fetchByStatus(value)}>执行完成</Radio.Button>
            </Radio.Group>
          </Col>
          <Col span={8}>
            <Search
              placeholder="请输入要查找的userId"
              style={{ width: 300 }}
              onSearch={value => this.search(value)}
              enterButton
            />
          </Col>
        </Row>

        <Table dataSource={goldUseRecordProds.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: goldUseRecordProds.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="用户Id"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="服务类型"
            dataIndex="goldUseFor"
            key="goldUseFor"
          />
          <Column
            title="订单ID"
            dataIndex="orderId"
            key="orderId"
          />
          <Column
            title="花费美元"
            dataIndex="orderRecharge"
            key="orderRecharge"
          />
          <Column
            title="花费金币数"
            dataIndex="coinNum"
            key="coinNum"
          />
          <Column
            title="链接"
            dataIndex="link"
            key="link"
          />
          <Column
            title="服务间隔"
            dataIndex="intervalTime"
            key="intervalTime"
          />
          <Column
            title="订单状态"
            dataIndex="orderStatus"
            key="orderStatus"
          />
          <Column
            title="购买时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />

        </Table>
      </div>
    );
  }
}

