import { Col, Form, Input, Row, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ goldCoin, loading }) => ({
  goldCoin,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class CoinUseTable extends PureComponent {

  state = {
    inputValue: '',
    status: 0,
    //0表示未搜索
    searchValue: 0,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'goldCoin/findAllGoldUseImg',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);


  }


  fetch = (pagination) => {
    const { dispatch } = this.props;
    if (this.state.status === 0 && this.state.searchValue === 0) {

      const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
      dispatch({
        type: 'goldCoin/findAllGoldUseImg',
        payload: {
          code: encryptParam(param),
        },
      });
    } else if (this.state.status === 1) {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1}';
        dispatch({
          type: 'goldCoin/findAllGoldUseImg',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 1,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldUseImg',
          payload: {
            code: encryptParam(param),
          },
        });
      }

    } else {
      if (this.state.searchValue === 0) {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0}';
        dispatch({
          type: 'goldCoin/findAllGoldUseImg',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + ',"status": 0,"userId": ' + this.state.searchValue + '}';
        dispatch({
          type: 'goldCoin/findAllGoldUseImg',
          payload: {
            code: encryptParam(param),
          },
        });
      }
    }


  };
  search = (inputValue) => {
    const { dispatch } = this.props;
    const param = '{"userId": ' + inputValue + ',"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseImg',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchPay = () => {
    this.setState({
      status: 1,
    });
    const { dispatch } = this.props;
    const param = '{"status": 1,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseImg',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchNotPay = () => {
    this.setState({
      status: 2,
    });
    const { dispatch } = this.props;
    const param = '{"status": 0,"pageNum": 1,"pageSize":100}';
    dispatch({
      type: 'goldCoin/findAllGoldUseImg',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {

    console.log('goldCoin-------', this.props.goldCoin);
    let goldUseRecords = this.props.goldCoin.goldUseRecord;
    return (
      <div>
        <Row>
          <Col span={12}>

          </Col>
          <Col span={12}>

          </Col>
        </Row>

        <Table dataSource={goldUseRecords.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: goldUseRecords.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="用户Id"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="服务类型"
            dataIndex="serviceType"
            key="serviceType"
            render={(serviceType) => {
              switch (serviceType) {
                case 0:
                  return '滤镜';
                case 1:
                  return '拼图';
                case 2:
                  return '形状';
                case 3:
                  return 'likes';
                case 4:
                  return 'followers';
                default:
                  return '未知';
              }
            }}
          />
          <Column
            title="服务名称"
            dataIndex="serviceName"
            key="serviceName"
          />
          <Column
            title="花费金币数"
            dataIndex="coinNum"
            key="coinNum"
          />
          <Column
            title="购买时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />

        </Table>
      </div>
    );
  }
}

