import { Form, Input, Modal } from 'antd';
import React, { PureComponent } from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const FormItem = Form.Item;

@connect(({ goldCoin, loading }) => ({
  goldCoin,
  loading: loading.models.goldCoin,
}))
@Form.create()
export default class UpdateServicePriceModal extends PureComponent {
  state = { visible: false };

  handleOk = (e) => {
    console.log('updateServicePriceModal------handleOk----------', e);
    this.props.valueToParent('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParent('cancel');
  };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;

        const param = '{"id":' + this.props.updateCoinId + ',"coinNum":' + values.coinNum + ',"coinPrice":' + values.coinPrice + '}';

        console.log('param:', param);
        dispatch({
          type: 'goldCoin/modifyCoinPrice',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        console.log('修改金币数量或价格-----------', err);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="修改金币售卖规则"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>

            <FormItem label="金币数量">
              {getFieldDecorator('coinNum', {
                rules: [{
                  required: true, message: '请输入金币数量',
                }],
              })(
                <Input placeholder="请输入金币数量"/>,
              )}
            </FormItem>


            <FormItem label="金币价格">
              {getFieldDecorator('coinPrice', {
                rules: [{
                  required: true, message: '请输入金币价格',
                }],
              })(
                <Input placeholder="请输入金币价格"/>,
              )}
            </FormItem>

          </Form>
        </Modal>
      </div>
    );
  }
}
