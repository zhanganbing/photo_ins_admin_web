import { Form, Input, Modal, Radio, Select } from 'antd';
import React, { PureComponent } from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
//下拉框的数据
let children = [];


@connect(({ configTable, loading }) => ({
  configTable,
  loading: loading.models.configTable,
}))
@Form.create()
export default class AddConfigModal extends PureComponent {
  state = { visible: false };

  componentDidMount() {
    const { dispatch } = this.props;

    const param = '{}';
    dispatch({
      type: 'configTable/getPackageNames',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  handleOk = (e) => {
    console.log('AddConfigModal------handleOk----------', e);
    this.props.valueToParent('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParent('cancel');
  };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;

        const param = '{"package_name":"' + values.packageName.label + '","platform":"' + values.platform + '","key":"' + values.configKey + '","value": "' + values.configValue + '"}';

        console.log('param:', param);
        dispatch({
          type: 'configTable/addOneConfig',
          payload: {
            code: encryptParam(param),
          },
          callback: (data) => {
            if (data.code === '0') {
              this.props.reload();
            }
          },
        });
      } else {
        console.log('服务价格添加错误-----------', err);
      }
    });
    //提交后应该刷新配置页

  };

  getChildren = (open) => {
    let packageNames = this.props.configTable.packageNameList;

    if (children.length <= 0) {

      for (let i = 0; i < packageNames.length; i++) {
        children.push(<Option key={i}>{packageNames[i]}</Option>);
      }
    }

    return children;
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="添加配置"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>

            <FormItem label="平台">
              {getFieldDecorator('platform', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <RadioGroup>
                  <Radio value={'ios'}>ios</Radio>
                  <Radio value={'android'}>android</Radio>
                </RadioGroup>,
              )}
            </FormItem>
            <FormItem label="包">
              {getFieldDecorator('packageName', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <Select labelInValue onDropdownVisibleChange={(open) => {
                  this.getChildren(open);
                }}>
                  {children}
                </Select>,
              )}
            </FormItem>
            <FormItem label="键">
              {getFieldDecorator('configKey', {
                rules: [{
                  required: true, message: '请输入配置键',
                }],
              })(
                <Input placeholder="请输入配置键"/>,
              )}
            </FormItem>


            <FormItem label="值">
              {getFieldDecorator('configValue', {
                rules: [{
                  required: true, message: '请输入配置值',
                }],
              })(
                <Input placeholder="请输入配置值"/>,
              )}
            </FormItem>


          </Form>
        </Modal>
      </div>
    );
  }
}
