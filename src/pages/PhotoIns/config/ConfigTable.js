import { Button, Form, Popconfirm, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import AddConfigModal from '@/pages/PhotoIns/config/AddConfigModal';
import moment from 'moment';
import UpdateConfigeModal from './UpdateConfigModal';

const { Column, ColumnGroup } = Table;

@connect(({ configTable, loading }) => ({
  configTable,
  loading: loading.models.configTable,
}))
@Form.create()
export default class ServicePriceTable extends PureComponent {

  state = {
    inputValue: '',
    addConfigModalVisible: false,
    updateConfigModalVisible: false,
    configKey: 0,
    flushComponent: false,
  };


  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'configTable/findAllConfig',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);

  }


  fetch = (pagination) => {
    const { dispatch } = this.props;

    const param = '{"pageNum":' + pagination.current + ',"pageSize":' + pagination.pageSize + '}';

    dispatch({
      type: 'configTable/findAllConfig',
      payload: {
        code: encryptParam(param),
      },
    });
  };

  valueToParent = (flag) => {
    if (flag === 'ok') {
      this.setState({
        addConfigModalVisible: true,
      });
    } else {
      this.setState({
        addConfigModalVisible: false,
      });
    }
  };
  valueToParent1 = (flag) => {
    if (flag === 'ok') {
      this.setState({
        updateConfigModalVisible: true,
      });
    } else {
      this.setState({
        updateConfigModalVisible: false,
      });
    }
  };

  changeVisible = () => {
    this.setState({
      addConfigModalVisible: true,
    });
  };
  changeVisibleForUpdate = (ccKey) => {
    this.setState({
      updateConfigModalVisible: true,
      configKey: ccKey,
    });
  };

  deleteColumn = (ccKey) => {
    console.log('deleteColumn----------------', ccKey);
    const { dispatch } = this.props;
    const param = '{"key": "' + ccKey + '"}';
    dispatch({
      type: 'configTable/delOneConfig',
      payload: {
        code: encryptParam(param),
      },
      callback: (data) => {
        if (data.code === '0') {
          this.reload();
        }
      },
    });

    //window.location.reload(true);

  };

  reload = () => {
    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'configTable/findAllConfig',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {
    let configList = this.props.configTable.configList;
    const { flushComponent } = this.state;
    return (
      <div>
        <Button type="primary" onClick={this.changeVisible}>添加配置</Button>
        <AddConfigModal reload={this.reload} valueToParent={this.valueToParent}
                        visible={this.state.addConfigModalVisible}/>
        <UpdateConfigeModal valueToParent1={this.valueToParent1} visible={this.state.updateConfigModalVisible}
                            configKey={this.state.configKey}/>

        <br/> <br/>
        <Table dataSource={configList.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: configList.total,
               }}
               flush={flushComponent}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />

          <Column
            title="键"
            dataIndex="ccKey"
            key="ccKey"

          />
          <Column
            title="值"
            dataIndex="ccValue"
            key="ccValue"
          />
          <Column
            title="包名"
            dataIndex="packageName"
            key="packageName"
          />
          <Column
            title="平台"
            dataIndex="platform"
            key="platform"
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="更新时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="操作"
            dataIndex="handle"
            key="handle"
            render={(text, record, index) => {
              const ccKey = record.ccKey;

              return (
                <div>
                  <Popconfirm title="确定删除？" onConfirm={() => this.deleteColumn(ccKey)}>
                    <Button type="danger">删除</Button>
                  </Popconfirm>
                  <Button type="primary" onClick={() => this.changeVisibleForUpdate(ccKey)}>修改</Button>
                </div>
              );
            }}
          />

        </Table>
      </div>
    )
      ;
  }


}

