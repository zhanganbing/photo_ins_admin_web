import { Form, Input, Modal } from 'antd';
import React, { PureComponent } from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const FormItem = Form.Item;

@connect(({ servicePrice, loading }) => ({
  servicePrice,
  loading: loading.models.servicePrice,
}))
@Form.create()
export default class UpdateConfigModal extends PureComponent {
  state = { visible: false };

  handleOk = (e) => {
    console.log('updateServicePriceModal------handleOk----------', e);
    this.props.valueToParent1('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParent1('cancel');
  };

  handleSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { dispatch } = this.props;

        const param = '{"key":"' + this.props.configKey + '","value":"' + values.configValue + '"}';

        console.log('param:', param);
        dispatch({
          type: 'configTable/updateOneConfig',
          payload: {
            code: encryptParam(param),
          },
        });
      } else {
        console.log('修改价格添加错误-----------', err);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="修改值"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>

            <FormItem label="值">
              {getFieldDecorator('configValue', {
                rules: [{
                  required: true, message: '请输入值',
                }],
              })(
                <Input placeholder="请输入值"/>,
              )}
            </FormItem>

          </Form>
        </Modal>
      </div>
    );
  }
}
