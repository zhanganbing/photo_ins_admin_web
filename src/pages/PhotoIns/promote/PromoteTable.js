import { Form, Radio, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import moment from 'moment';

const { Column, ColumnGroup } = Table;

@connect(({ promote, loading }) => ({
  promote,
  loading: loading.models.promote,
}))
@Form.create()
export default class PromoteTable extends PureComponent {

  state = {
    inputValue: '',
    shareOrCommentFlag: 0,
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum": 1,"pageSize": 10}';

    dispatch({
      type: 'promote/findAllPromote',
      payload: {
        code: encryptParam(param),
      },
    });
  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);


  }


  fetch = (pagination) => {
    const { dispatch } = this.props;
    const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
    dispatch({
      type: 'promote/findAllPromote',
      payload: {
        code: encryptParam(param),
      },
    });
  };

  fetchComment = () => {
    this.setState({
      shareOrCommentFlag: 2,
    });
    const { dispatch } = this.props;
    const param = '{"isCommented": 1}';
    dispatch({
      type: 'promote/findAllPromote',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchShare = () => {
    this.setState({
      shareOrCommentFlag: 1,
    });
    const { dispatch } = this.props;
    const param = '{"isShared": 1}';
    dispatch({
      type: 'promote/findAllPromote',
      payload: {
        code: encryptParam(param),
      },
    });
  };
  fetchShareAndComment = () => {
    this.setState({
      shareOrCommentFlag: 3,
    });
    const { dispatch } = this.props;
    const param = '{"isShared": 1,"isCommented": 1}';
    dispatch({
      type: 'promote/findAllPromote',
      payload: {
        code: encryptParam(param),
      },
    });
  };



  render() {

    console.log('promote-------', this.props.promote);
    let promoteList = this.props.promote.promoteList;
    return (
      <div>
        <Radio.Group buttonStyle="solid">
          <Radio.Button value="share" onClick={this.fetchShare}>已分享</Radio.Button>
          <Radio.Button value="comment" onClick={this.fetchComment}>已评论</Radio.Button>
          <Radio.Button value="comment_share" onClick={this.fetchShareAndComment}>已评论已分享</Radio.Button>
        </Radio.Group>

        <br />
        <br />
        <Table dataSource={promoteList.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: promoteList.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="用户Id"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="是否分享"
            dataIndex="isShared"
            key="isShared"
            render={(record) => {
              if (record === 1) {
                return '是';
              }
              return '否';
            }}
          />
          <Column
            title="是否评论"
            dataIndex="isCommented"
            key="isCommented"
            render={(record) => {
              if (record === 1) {
                return '是';
              }
              return '否';
            }}
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="更新时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />

        </Table>
      </div>
    );
  }
}

