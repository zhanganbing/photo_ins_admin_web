import { Form, Input, Modal, Radio, Select } from 'antd';
import React, { PureComponent } from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
//下拉框的数据
let children = [];

@connect(({ servicePrice, configTable, loading }) => ({
  servicePrice,
  configTable,
  loading: loading.models.servicePrice,
}))
@Form.create()
export default class AddServicePriceModal extends PureComponent {
  state = { visible: false };

  componentDidMount() {
    const { dispatch } = this.props;

    const param = '{}';
    dispatch({
      type: 'configTable/getPackageNames',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  handleOk = (e) => {
    console.log('addServicePriceModal------handleOk----------', e);
    this.props.valueToParent('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParent('cancel');
  };

  getChildren = (open) => {
    let packageNames = this.props.configTable.packageNameList;

    if (children.length <= 0) {

      for (let i = 0; i < packageNames.length; i++) {
        children.push(<Option key={i}>{packageNames[i]}</Option>);
      }
    }

    return children;
  };

  handleSubmit = () => {
    this.props.form.validateFields((err,values) => {
      if(!err){
        const { dispatch } = this.props;

        const param = '{"package_name":"' + values.packageName + '","platform":"' + values.platform + '","serviceType":' + values.serviceType + ',"serviceNum":' + values.serviceNum + ',"coin":' + values.servicePrice + '}';

        console.log("param:",param);
        dispatch({
          type: 'servicePrice/saveServicePrice',
          payload: {
            code: encryptParam(param),
          },
          callback: (response) => {
            if (response.code === '0') {
              this.props.reload();
            }
          },
        });
      }else{
        console.log("服务价格添加错误-----------", err);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="添加服务价格"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>

            <FormItem label="平台">
              {getFieldDecorator('platform', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <RadioGroup>
                  <Radio value={'ios'}>ios</Radio>
                  <Radio value={'android'}>android</Radio>
                </RadioGroup>,
              )}
            </FormItem>
            <FormItem label="包">
              {getFieldDecorator('packageName', {
                rules: [{
                  required: true, message: '请输入包名',
                }],
              })(
                <Select labelInValue onDropdownVisibleChange={(open) => {
                  this.getChildren(open);
                }}>
                  {children}
                </Select>,
              )}
            </FormItem>
            <FormItem label="服务类型">
              {getFieldDecorator('serviceType', {
                rules: [{
                  required: true, message: '请输入服务类型',
                }],
              })(
                <Input placeholder="请输入服务类型"/>,
              )}
            </FormItem>


            <FormItem label="服务数量">
              {getFieldDecorator('serviceNum', {
                rules: [{
                  required: true, message: '请输入服务数量',
                }],
              })(
                <Input placeholder="请输入服务数量"/>,
              )}
            </FormItem>


            <FormItem label="服务价格">
              {getFieldDecorator('servicePrice', {
                rules: [{
                  required: true, message: '请输入服务价格',
                }],
              })(
                <Input placeholder="请输入服务价格"/>,
              )}
            </FormItem>

          </Form>
        </Modal>
      </div>
    );
  }
}
