import { Button, Form, Popconfirm, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import AddServicePriceModal from '@/pages/PhotoIns/service/AddServicePriceModal';
import UpdateServicePriceModal from '@/pages/PhotoIns/service/UpdateServicePriceModal';
import moment from 'moment';

const { Column, ColumnGroup } = Table;

@connect(({ servicePrice, loading }) => ({
  servicePrice,
  loading: loading.models.servicePrice,
}))
@Form.create()
export default class ServicePriceTable extends PureComponent {

  state = {
    inputValue: '',
    addPriceModalVisible: false,
    updatePriceModalVisible: false,
    updatePriceId: 0,
  };


  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'servicePrice/findAllServicePrice',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);

  }


  fetch = (pagination) => {
    const { dispatch } = this.props;
    const param = '{"pageNum": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';
    dispatch({
      type: 'servicePrice/findAllServicePrice',
      payload: {
        code: encryptParam(param),
      },
    });
  };

  valueToParent = (flag) => {
    if (flag === 'ok') {
      this.setState({
        addPriceModalVisible: true,
      });
    } else {
      this.setState({
        addPriceModalVisible: false,
      });
    }
  };
  valueToParent1 = (flag) => {
    if (flag === 'ok') {
      this.setState({
        updatePriceModalVisible: true,
      });
    } else {
      this.setState({
        updatePriceModalVisible: false,
      });
    }
  };

  changeVisible = () => {
    this.setState({
      addPriceModalVisible: true,
    });
  };
  changeVisibleForUpdate = (id) => {
    this.setState({
      updatePriceModalVisible: true,
      updatePriceId: id,
    });
  };

  deleteColumn = (id) => {
    console.log('deleteColumn----------------', id);
    const { dispatch } = this.props;
    const param = '{"id": ' + id + '}';
    dispatch({
      type: 'servicePrice/deleteServicePrice',
      payload: {
        code: encryptParam(param),
      },
      callback: (response) => {
        if (response.code === '0') {
          this.reload();
        }
      },
    });
  };

  reload = () => {
    const { dispatch } = this.props;

    const param = '{"pageNum":1,"pageSize":10}';

    dispatch({
      type: 'servicePrice/findAllServicePrice',
      payload: {
        code: encryptParam(param),
      },
    });
  };

  render() {
    console.log('servicePrice-------', this.props.servicePrice);
    let servicePriceList = this.props.servicePrice.servicePriceList;
    return (
      <div>
        <Button type="primary" onClick={this.changeVisible}>添加价格</Button>
        <AddServicePriceModal reload={this.reload} valueToParent={this.valueToParent}
                              visible={this.state.addPriceModalVisible}/>
        <UpdateServicePriceModal valueToParent1={this.valueToParent1} visible={this.state.updatePriceModalVisible}
                                 updatePriceId={this.state.updatePriceId}/>
        <br/> <br/>
        <Table dataSource={servicePriceList.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: servicePriceList.total,
               }}
        >

          <Column
            title="ID"
            dataIndex="id"
            key="id"
          />
          <Column
            title="服务类型"
            dataIndex="serviceType"
            key="serviceType"
            render={(record) => {
              switch (record) {
                case 3:
                  return '点赞';
                case 4:
                  return '粉丝';
                case 5:
                  return '浏览量';
                default:
                  return 'nothing';
              }

            }}
          />
          <Column
            title="服务数量"
            dataIndex="serviceNum"
            key="serviceNum"

          />
          <Column
            title="金币价格"
            dataIndex="coins"
            key="coins"
          />
          <Column
            title="包名"
            dataIndex="packageName"
            key="packageNames"
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="更新时间"
            dataIndex="updateTime"
            key="updateTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />
          <Column
            title="操作"
            dataIndex="handle"
            key="handle"
            render={(text, record, index) => {
              const id = record.id;

              return (
                <div>
                  <Popconfirm title="确定删除？" onConfirm={() => this.deleteColumn(id)}>
                    <Button type="danger">删除</Button>
                  </Popconfirm>
                  <Button type="primary" onClick={() => this.changeVisibleForUpdate(id)}>修改</Button>
                </div>
              );
            }}
          />

        </Table>
      </div>
    )
      ;
  }


}

