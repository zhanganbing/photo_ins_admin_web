import { Modal, Input, Form, DatePicker, Select } from 'antd';
import { PureComponent } from 'react';
import React from 'react';
import { encryptParam } from '@/utils/EncryptUtils';
import { connect } from 'dva';

const FormItem = Form.Item;

@connect(({ servicePrice, loading }) => ({
  servicePrice,
  loading: loading.models.servicePrice,
}))
@Form.create()
export default class UpdateServicePriceModal extends PureComponent {
  state = { visible: false };

  handleOk = (e) => {
    console.log('updateServicePriceModal------handleOk----------', e);
    this.props.valueToParent1('cancel');
    this.handleSubmit();
  };

  handleCancel = (e) => {
    console.log(e);
    this.props.valueToParent1('cancel');
  };

  handleSubmit = () => {
    this.props.form.validateFields((err,values) => {
      if(!err){
        const { dispatch } = this.props;

        const param = '{"id":'+this.props.updatePriceId+',"serviceNum":'+values.serviceNum+',"coin":'+values.servicePrice+'}';

        console.log("param:",param);
        dispatch({
          type: 'servicePrice/modifyServicePrice',
          payload: {
            code: encryptParam(param),
          },
        });
      }else{
        console.log("修改价格添加错误-----------", err);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Modal
          title="修改服务价格"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >

          <Form onSubmit={this.handleSubmit}>

            <FormItem label="服务数量">
              {getFieldDecorator('serviceNum', {
                rules: [{
                  required: true, message: '请输入服务数量',
                }],
              })(
                <Input placeholder="请输入服务数量"/>,
              )}
            </FormItem>


            <FormItem label="服务价格">
              {getFieldDecorator('servicePrice', {
                rules: [{
                  required: true, message: '请输入服务价格',
                }],
              })(
                <Input placeholder="请输入服务价格"/>,
              )}
            </FormItem>

          </Form>
        </Modal>
      </div>
    );
  }
}
