import { Form, Input, Table } from 'antd';
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { encryptParam } from '@/utils/EncryptUtils';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import moment from 'moment';

const Search = Input.Search;

const { Column, ColumnGroup } = Table;

@connect(({ userTable, loading }) => ({
  userTable,
  loading: loading.models.userTable,
}))
@Form.create()
class UserTable extends PureComponent {

  state = {
    inputValue: '',
  };

  componentDidMount() {

    const { dispatch } = this.props;

    const param = '{"current":1,"pageSize":10}';

    dispatch({
      type: 'userTable/findUsers',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  fetch(pagination) {

    const { dispatch } = this.props;

    console.log('------------fetch----props-----------------', this.props);
    console.log('------------fetch----pagination-----------------', pagination);
    const param = '{"current": ' + pagination.current + ',"pageSize": ' + pagination.pageSize + '}';

    dispatch({
      type: 'userTable/findUsers',
      payload: {
        code: encryptParam(param),
      },
    });

  }

  onShowSizeChange(current, pageSize) {

    console.log('------------onShowSizeChange--------------------', current, pageSize);


  }

  fetchMore = (inputValue) => {
    const { dispatch } = this.props;
    const param = '{"userId": ' + inputValue + ',"userName": ' + inputValue + '}';
    dispatch({
      type: 'userTable/findOneUser',
      payload: {
        code: encryptParam(param),
      },
    });
  };


  render() {
    console.log('userList-------', this.props.userTable);
    let users = this.props.userTable.userList;
    return (
      <div>

        <Row>
          <Col span={8}>
            <Search
              placeholder="请输入要查找的关键字"
              style={{ width: 300 }}
              onSearch={value => this.fetchMore(value)}
              enterButton
            />
          </Col>
          <Col span={8}> </Col>
          <Col span={8}> </Col>
        </Row>

        <Table dataSource={users.list}
               onChange={(pagination) => this.fetch(pagination)}
               onShowSizeChange={this.onShowSizeChange()}
               pagination={{
                 total: users.total,
               }}
        >

          <Column
            title="用户ID"
            dataIndex="userId"
            key="userId"
          />
          <Column
            title="用户名称"
            dataIndex="userName"
            key="userName"
          />
          <Column
            title="用户邮箱"
            dataIndex="userEmail"
            key="userEmail"
          />
          <Column
            title="用户头像"
            dataIndex="userAvatar"
            key="userAvatar"
            render={(text) => {
              return <img src={text} height={60} width={60} alt={'avatar'}/>;
            }}
          />
          <Column
            title="用户金币数"
            dataIndex="userGoldNum"
            key="userGoldNum"
          />
          <Column
            title="是否Instagram"
            dataIndex="isIns"
            key="isIns"
          />
          <Column
            title="包名"
            dataIndex="packageName"
            key="packageName"
          />
          <Column
            title="创建时间"
            dataIndex="createTime"
            key="createTime"
            render={(record) => {
              return moment(record).format('YYYY-MM-DD');
            }
            }
          />

        </Table>
      </div>
    );
  }
}

export default UserTable;
