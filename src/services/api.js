import { stringify } from 'qs';
import request from '@/utils/request';

const apiHost = 'http://api.easy-ai.com/ins';

//const apiHost = 'http://localhost:8081/ins';

/** ************************************示例请求****************************************** */
export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    data: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    data: {
      ...restParams,
      method: 'post',
    },
  });
}

/** ************************************示例请求****************************************** */
// 分页查询所有用户
export async function findAll(params) {
  return request(`${apiHost}/admin/user/get_all_user`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

export async function adminLogin(params) {
  // console.log(params);
  return request(`${apiHost}/admin/login`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 单查一个用户
export async function findByUser(params) {
  return request(`${apiHost}/admin/user/find_by_user`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 分页查询所有验证码
export async function findAllCaptcha(params) {
  return request(`${apiHost}/admin/captcha/qryCaptcha`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 分页查询所有推广记录
export async function findAllPromote(params) {
  return request(`${apiHost}/admin/promote/qryPromoteRecord`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 分页查询所有服务价格记录
export async function findAllServicePrice(params) {
  return request(`${apiHost}/admin/servicePrice/qryServicePrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 添加服务价格
export async function saveServicePrice(params) {
  return request(`${apiHost}/admin/servicePrice/addServicePrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 删除一条服务价格
export async function deleteOneServicePrice(params) {
  return request(`${apiHost}/admin/servicePrice/deleteServicePrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 修改一条服务价格
export async function modifyOneServicePrice(params) {
  return request(`${apiHost}/admin/servicePrice/modifyServicePrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 查询金币购买记录
export async function queryGoldCoinBuyRecord(params) {
  return request(`${apiHost}/admin/gold/qryGoldAdd`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 修改金币购买状态
export async function modifyGoldCoinBuyStatus(params) {
  return request(`${apiHost}/admin/gold/modifyGoldAdd`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 删除金币购买记录
export async function deleteGoldCoinBuyRecord(params) {
  return request(`${apiHost}/admin/gold/deleteGoldAdd`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 查询金币购买服务记录
export async function qryGoldUseImg(params) {
  return request(`${apiHost}/admin/gold/qryGoldUseImg`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 查询金币购买服务记录（生产环境）
export async function qryGoldUseProd(params) {
  return request(`${apiHost}/admin/gold/qryGoldUseProd`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 金币价格表
export async function qrycoinPrice(params) {
  return request(`${apiHost}/admin/gold/qrycoinPrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 删除一行金币价格记录
export async function deleteCoinPrice(params) {
  return request(`${apiHost}/admin/gold/deleteCoinPrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 新增一行金币价格记录
export async function insertCoinPrice(params) {
  return request(`${apiHost}/admin/gold/insertCoinPrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 修改一行金币价格记录
export async function modifyCoinPrice(params) {
  return request(`${apiHost}/admin/gold/modifyCoinPrice`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 查询所有配置项
export async function queryAllConfig(params) {
  return request(`${apiHost}/admin/config/qryAllConfig`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 新增配置
export async function addConfig(params) {
  return request(`${apiHost}/admin/config/addConfig`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 修改配置（值）
export async function updateConfig(params) {
  return request(`${apiHost}/admin/config/updateConfig`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 删除配置
export async function delConfig(params) {
  return request(`${apiHost}/admin/config/delConfig`, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

// 查询包名
export async function findPackageNames() {
  return request(`${apiHost}/admin/package/get`);

}

