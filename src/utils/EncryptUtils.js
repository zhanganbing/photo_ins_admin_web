function encryptParam(paramJson) {
  //引包的一种方式
  let CryptoJS = require('crypto-js');
  let str = paramJson;
  // 密钥 16 位   与后端一致
  let key = 'chihewanleshijie';

  //后端用的key是 byte数组，转一下
  key = CryptoJS.enc.Utf8.parse(key);

  let encrypted = CryptoJS.AES.encrypt(str, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });

  // encrypted生成的是一个对象，通过ciphertext拿到密文
  encrypted = encrypted.ciphertext.toString();
  //转成16进制
  let encryptedHexStr = CryptoJS.enc.Hex.parse(encrypted);

  //base64编码
  let base64 = CryptoJS.enc.Base64.stringify(encryptedHexStr);

  //返回的密文才可被后端解析
  return base64;
}

/*
function decryptParam(paramJson) {
  let CryptoJS = require('crypto-js');
  // 密钥 16 位   与后端一致
  let keyStr = '1234567890123456';
  let key = CryptoJS.enc.Utf8.parse(keyStr);

  let baseResult = CryptoJS.enc.Base64.parse(paramJson);
  let ciphertext = CryptoJS.enc.Base64.stringify(baseResult);
  var decrypt = CryptoJS.AES.decrypt(ciphertext, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return JSON.parse(decryptedStr);
}*/
function decryptParam(word) {
  let CryptoJS = require('crypto-js');
  // 密钥 16 位   与后端一致
  let key = 'chihewanleshijie';
  //后端用的key是 byte数组，转一下
  key = CryptoJS.enc.Utf8.parse(key);

  let bytes = CryptoJS.AES.decrypt(word.toString(), key, {
    //iv: key,
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  let decryptResult = bytes.toString(CryptoJS.enc.Utf8);
  return decryptResult.toString();
}

export {
  encryptParam,
  decryptParam,
};


