import fetch from 'dva/fetch';
import { stringify } from 'qs';
import { message, notification } from 'antd';
import router from 'umi/router';
import { getPageQuery, getCookie } from '@/utils/utils';

// const apiHost = 'http://192.168.1.101:8080/web_api_war_exploded';
// const apiHost = 'http://192.168.1.104:8080';
// const apiHost = 'http://testapi.ljd.leyongleshi.com';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const errortext = codeMessage[response.status] || response.statusText;
  notification.error({
    message: `请求错误 ${response.status}: ${response.url}`,
    description: errortext,
  });
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  // url = apiHost + url;
  // console.log(url);
  const token = getCookie('token');
  const userName = getCookie('userName');

  const defaultOptions = {
    // credentials: 'include',
    headers: {
      token,
      userName,
    },
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    if (!(newOptions.body instanceof FormData)) {
      newOptions.headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...newOptions.headers,
      };
      newOptions.body = JSON.stringify(newOptions.data);
    } else {
      newOptions.headers = {
        Accept: 'application/x-www-form-urlencoded',
        ...newOptions.headers,
      };
    }
  }

  // return fetch(configs[process.env.API_ENV].API_SERVER + url, newOptions)
  return fetch(`${url}`, newOptions)
    .then(checkStatus)
    .then(response => {
      if (newOptions.method === 'DELETE' || response.status === 204) {
        return response.text();
      }
      const result = response.json();

      result.then(r => {
        if (r.msg) {
          if (r.code !== '0') {
            if (r.msg.trim().toLowerCase() === '请先登录') {
              router.push('/user/login');
              message.error('无效的授权码，请重新登陆');
            } else {
              message.error(r.msg);
            }
          } else if (r.data === true) {
            message.success('操作成功');
          }
        }
      });
      return result;
    })
    .catch(e => {
      console.log(e);
      const status = e.name;
      if (status === 401) {
        return;
      }
      if (status === 403) {
        return;
      }
      if (status <= 504 && status >= 500) {
        return;
      }
      if (status >= 404 && status < 422) {
      }
    });
}
